# dockerized crux-pipeline

Docker container for Kaipo's [crux-pipeline](https://bitbucket.org/maccosslab/crux-pipeline)


### Getting started

```
docker run -it --rm crux-pipeline python /app/pipeline.py --help
```
